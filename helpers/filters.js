export function filterProducts(filter, products) {
  let filteredList = [...products];

  // Filter category
  if (filter.category !== "all") {
    const filtered = filteredList.filter(
      (product) => product.category.name === filter.category
    );
    filteredList = filtered;
  }

  // Filter subCategory
  if (filter.subCategory !== "all") {
    const filtered = filteredList.filter(
      (product) =>
        product.sub_category && product.sub_category.name === filter.subCategory
    );
    filteredList = filtered;
  }
  return filteredList;
}

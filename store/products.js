import * as Filters from "~/helpers/filters";

export const state = () => ({
  title: "",
  products: [],
  filteredProducts: [],
  product: {},
  filter: {
    category: "all",
    subCategory: "all",
  },
});

export const getters = {
  getTitle(state) {
    return state.title;
  },
  getProducts(state) {
    return state.products;
  },
  getFilteredProducts(state) {
    return state.filteredProducts;
  },
  getProduct(state) {
    return state.product;
  },
};

export const mutations = {
  setTitle(state, title) {
    state.title = title;
  },
  setProduct(state, product) {
    state.product = product;
  },
  setProducts(state, products) {
    state.products = products;
  },
  setFilteredProducts(state, products) {
    state.filteredProducts = products;
  },
  setFilterCategory(state, category) {
    state.filter.category = category;
  },
  setFilterSubCategory(state, subCategory) {
    state.filter.subCategory = subCategory;
  },
  filterProducts(state) {
    const products = [...state.products];
    state.filteredProducts = products;
    state.filteredProducts = Filters.filterProducts(state.filter, products);
  },
};

export const actions = {
  async fetchAllProducts({ commit }) {
    const products = await this.$strapi.$products.find();
    await commit("setTitle", "Tous les produits");
    await commit("setProducts", products);
    await commit("setFilteredProducts", products);
  },
  async filterCategory({ commit, dispatch }, category) {
    await commit("setTitle", category);
    await commit("setFilterCategory", category);
    dispatch("filterProducts");
  },
  async filterSubCategory({ commit, dispatch }, subCategory) {
    await commit("setTitle", subCategory);
    await commit("setFilterSubCategory", subCategory);
    dispatch("filterProducts");
  },
  async filterProducts({ commit }) {
    await commit("filterProducts");
  },
};

const getMediaUrl = (url) => {
  return url ? `${process.env.API_URL}${url}` : null;
};

const generateUrl = (data) => {
  let url = "";
  if (!data.category) {
    url = `/categorie/${data.slug}`;
  } else if (!data.sub_category && data.sub_category !== null) {
    url = `/sous-categorie/${data.slug}`;
  } else {
    url = `/produit/${data.slug}`;
  }
  return url;
};

export default {
  getMediaUrl,
  generateUrl,
};
